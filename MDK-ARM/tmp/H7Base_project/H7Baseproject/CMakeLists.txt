# CMSIS Build CMakeLists generated on 2024-05-04T11:07:00

cmake_minimum_required(VERSION 3.22)

# Target options

set(TARGET H7Base_project)
set(CPU Cortex-M7)
set(PRJ_DIR "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/MDK-ARM")
set(OUT_DIR "C:/Users/Administrator/Desktop/H7Base_project/H7Base_project/MDK-ARM/out/H7Base_project/H7Baseproject")
set(INT_DIR "C:/Users/Administrator/Desktop/H7Base_project/H7Base_project/MDK-ARM/tmp/H7Base_project/H7Baseproject")
set(FPU DP_FPU)
set(BYTE_ORDER Little-endian)
set(OPTIMIZE size)
set(DEBUG on)
set(AS_FLAGS_GLOBAL "-masm=auto")
set(CC_FLAGS_GLOBAL "-std=c99 -fno-rtti -funsigned-char -fshort-enums -fshort-wchar -ffunction-sections -Wno-packed -Wno-missing-variable-declarations -Wno-missing-prototypes -Wno-missing-noreturn -Wno-sign-conversion -Wno-nonportable-include-path -Wno-reserved-id-macro -Wno-unused-macros -Wno-documentation-unknown-command -Wno-documentation -Wno-license-management -Wno-parentheses-equality")
set(CXX_FLAGS_GLOBAL "-xc++ -std=c++11 -fno-rtti -funsigned-char -fshort-enums -fshort-wchar -ffunction-sections -Wno-packed -Wno-missing-variable-declarations -Wno-missing-prototypes -Wno-missing-noreturn -Wno-sign-conversion -Wno-nonportable-include-path -Wno-reserved-id-macro -Wno-unused-macros -Wno-documentation-unknown-command -Wno-documentation -Wno-license-management -Wno-parentheses-equality")
set(LD_FLAGS_GLOBAL "--map --load_addr_map_info --xref --callgraph --symbols --info sizes --info totals --info unused --info veneers --strict --summary_stderr --info summarysizes")
set(LD_SCRIPT "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/MDK-ARM/H7Base_project_H7Baseproject.sct")

set(DEFINES
  STM32H743xx
  _RTE_
  USE_HAL_DRIVER
)

set(INC_PATHS
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Core/Inc"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Drivers/STM32H7xx_HAL_Driver/Inc"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Drivers/STM32H7xx_HAL_Driver/Inc/Legacy"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Drivers/CMSIS/Device/ST/STM32H7xx/Include"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Drivers/CMSIS/Include"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/MDK-ARM/RTE/_H7Baseproject"
  "C:/Users/Administrator/AppData/Local/Arm/packs/ARM/CMSIS/5.9.0/CMSIS/Core/Include"
  "C:/Users/Administrator/AppData/Local/Arm/packs/Keil/STM32H7xx_DFP/3.1.1/Drivers/STM32H7xx_HAL_Driver/Inc"
  "C:/Users/Administrator/AppData/Local/arm/packs/Keil/STM32H7xx_DFP/3.1.1/Drivers/CMSIS/Device/ST/STM32H7xx/Include"
)

set(AS_ARM_SRC_FILES
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/MDK-ARM/startup_stm32h743xx.s"
)

set(CC_SRC_FILES
  "C:/Users/Administrator/AppData/Local/Arm/packs/Keil/STM32H7xx_DFP/3.1.1/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal.c"
  "C:/Users/Administrator/AppData/Local/Arm/packs/Keil/STM32H7xx_DFP/3.1.1/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_dma.c"
  "C:/Users/Administrator/AppData/Local/Arm/packs/Keil/STM32H7xx_DFP/3.1.1/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_dma_ex.c"
  "C:/Users/Administrator/AppData/Local/Arm/packs/Keil/STM32H7xx_DFP/3.1.1/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_usart.c"
  "C:/Users/Administrator/AppData/Local/Arm/packs/Keil/STM32H7xx_DFP/3.1.1/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_usart_ex.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Core/Src/gpio.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Core/Src/main.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Core/Src/stm32h7xx_hal_msp.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Core/Src/stm32h7xx_it.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Core/Src/system_stm32h7xx.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_cortex.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_dma.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_dma_ex.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_exti.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_flash.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_flash_ex.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_gpio.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_hsem.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_i2c.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_i2c_ex.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_mdma.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_pwr.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_pwr_ex.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_rcc.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_rcc_ex.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_tim.c"
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_tim_ex.c"
)

set(PRE_INC_GLOBAL
  "c:/Users/Administrator/Desktop/H7Base_project/H7Base_project/MDK-ARM/RTE/_H7Baseproject/Pre_Include_Global.h"
)

# Toolchain config map

set(REGISTERED_TOOLCHAIN_ROOT "c:/Users/Administrator/.vcpkg/artifacts/2139c4c6/compilers.arm.armclang/6.22.0/bin")
set(REGISTERED_TOOLCHAIN_VERSION "6.22.0")
set(TOOLCHAIN_VERSION_MIN "6.22.0")
include ("C:/Users/Administrator/.vcpkg/artifacts/2139c4c6/tools.open.cmsis.pack.cmsis.toolbox/2.3.0/etc/AC6.6.18.0.cmake")
include ("C:/Users/Administrator/.vcpkg/artifacts/2139c4c6/tools.open.cmsis.pack.cmsis.toolbox/2.3.0/etc/CMSIS-Build-Utils.cmake")

# Setup project

project(${TARGET} LANGUAGES AS_ARM C)

cbuild_get_running_toolchain(TOOLCHAIN_ROOT TOOLCHAIN_VERSION C)

# Global Flags

set(CMAKE_AS_ARM_FLAGS "${AS_ARM_CPU} ${AS_ARM_BYTE_ORDER} ${AS_ARM_DEFINES} ${AS_ARM_OPTIONS_FLAGS} ${AS_ARM_FLAGS} ${AS_FLAGS_GLOBAL}")
cbuild_get_system_includes(CC_SYS_INC_PATHS_LIST CC_SYS_INC_PATHS)
set(CMAKE_C_FLAGS "${CC_CPU} ${CC_BYTE_ORDER} ${CC_DEFINES} ${CC_OPTIONS_FLAGS} ${CC_FLAGS} ${CC_FLAGS_GLOBAL} ${CC_SYS_INC_PATHS}")
set(CMAKE_C_LINK_FLAGS "${LD_CPU} ${_LS}\"${LD_SCRIPT}\" ${LD_FLAGS_GLOBAL} ${LD_OPTIONS_FLAGS} ${LD_FLAGS}")

foreach(ENTRY ${PRE_INC_GLOBAL})
  string(APPEND CMAKE_C_FLAGS " ${_PI}\"${ENTRY}\"")
endforeach()

# Local Flags

foreach(SRC ${AS_ARM_SRC_FILES})
  set_source_files_properties(${SRC} PROPERTIES LANGUAGE AS_ARM)
endforeach()

# Compilation Database

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
add_custom_target(database COMMAND ${CMAKE_COMMAND} -E copy_if_different "${INT_DIR}/compile_commands.json" "${OUT_DIR}")

# Setup Target

add_executable(${TARGET} ${AS_ARM_SRC_FILES} ${CC_SRC_FILES})
set_target_properties(${TARGET} PROPERTIES PREFIX "" SUFFIX ".axf" OUTPUT_NAME "H7Base_project")
set_target_properties(${TARGET} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${OUT_DIR} LINK_DEPENDS ${LD_SCRIPT})
target_include_directories(${TARGET} PUBLIC ${INC_PATHS})
