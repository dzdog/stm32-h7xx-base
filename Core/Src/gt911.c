#include "gt911.h"

void GT911_Init(uint8_t addr)
{
   switch(addr)
   {
        case 0xBA:
            HAL_GPIO_WritePin(GT911_GPIO_PROT, GT911_INT_PIN, GPIO_PIN_RESET); 
            HAL_GPIO_WritePin(GT911_GPIO_PROT, GT911_RST_PIN, GPIO_PIN_RESET); 
            HAL_Delay(40);
            HAL_GPIO_WritePin(GT911_GPIO_PROT, GT911_RST_PIN, GPIO_PIN_SET); 
            HAL_GPIO_WritePin(GT911_GPIO_PROT, GT911_INT_PIN, GPIO_PIN_RESET);
            HAL_Delay(40);
            HAL_GPIO_WritePin(GT911_GPIO_PROT, GT911_INT_PIN, GPIO_PIN_RESET);
            HAL_Delay(40);
            HAL_GPIO_WritePin(GT911_GPIO_PROT, GT911_INT_PIN, GPIO_PIN_SET);
        break;
        case 0x28:
            HAL_GPIO_WritePin(GT911_GPIO_PROT, GT911_RST_PIN, GPIO_PIN_RESET); 
            HAL_GPIO_WritePin(GT911_GPIO_PROT, GT911_INT_PIN, GPIO_PIN_SET);
            HAL_Delay(40);
            HAL_GPIO_WritePin(GT911_GPIO_PROT, GT911_RST_PIN, GPIO_PIN_SET); 
            HAL_Delay(10);
            //HAL_GPIO_WritePin(GT911_GPIO_PROT, GT911_INT_PIN, GPIO_PIN_SET);
            //HAL_Delay(30);
            HAL_GPIO_WritePin(GT911_GPIO_PROT, GT911_INT_PIN, GPIO_PIN_RESET);

            HAL_Delay(55);
            //HAL_GPIO_WritePin(GT911_GPIO_PROT, GT911_INT_PIN, GPIO_PIN_SET);
        break;

        default:
            printf("GT911_Init error\r\n");
        break;
   }
}

uint8_t GT911_Write(uint16_t reg, uint8_t *data, uint8_t len)
{
    uint8_t i = 0;
    uint8_t ret = 0;
    IIC_Start();
    IIC_Send_Byte(GT911_CMD_W);
    IIC_Wait_Ack();
    IIC_Send_Byte(reg >> 8); /*发送高八位*/
    IIC_Wait_Ack();
    IIC_Send_Byte(reg & 0xFF); /*发送低八位*/
    IIC_Wait_Ack();
    for(i = 0; i < len; i++)
    {
        IIC_Send_Byte(data[i]);
        ret = IIC_Wait_Ack();
        if(ret)
        break;
    }
    IIC_Stop();
    return ret;
}

void GT911_Read(uint16_t reg, uint8_t *data, uint8_t len)
{
    uint8_t i = 0;

    IIC_Start();
    IIC_Send_Byte(GT911_CMD_W);
    IIC_Wait_Ack();
    IIC_Send_Byte((uint8_t)(reg >> 8)); /*发送高八位*/
    IIC_Wait_Ack();
    IIC_Send_Byte((uint8_t)(reg & 0xFF)); /*发送低八位*/
    IIC_Wait_Ack();
    IIC_Stop();

    IIC_Start();
    IIC_Send_Byte(GT911_CMD_R);
    IIC_Wait_Ack();
    for(i = 0; i < len; i++)
    {
        data[i] = IIC_Read_Byte(0);
        IIC_Ack();
    }
    data[i] = IIC_Read_Byte(0);
    IIC_NAck();
    IIC_Stop();
}




