#include "iic.h"
void SDA_IN(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    GPIO_InitStruct.Pin = GT911_SDA;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
};


void SDA_OUT(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    GPIO_InitStruct.Pin = GT911_SDA;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

void IIC_Set_Scl(uint8_t state)
{
    if(state)
        HAL_GPIO_WritePin(GPIOB, GT911_SCL, GPIO_PIN_SET);
    else
        HAL_GPIO_WritePin(GPIOB, GT911_SCL, GPIO_PIN_RESET);
}

void IIC_Set_Sda(uint8_t state)
{
    if(state)
        HAL_GPIO_WritePin(GPIOB, GT911_SDA, GPIO_PIN_SET);
    else
        HAL_GPIO_WritePin(GPIOB, GT911_SDA, GPIO_PIN_RESET);
}


uint8_t IIC_Read_Sda(void)
{
    return HAL_GPIO_ReadPin(GPIOB, GT911_SDA); 
}
void IIC_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    __HAL_RCC_GPIOB_CLK_ENABLE();
    GPIO_InitStruct.Pin = GT911_SCL | GT911_SDA;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Pull = GPIO_PULLUP; 
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    IIC_Set_Scl(1);
    IIC_Set_Sda(1);
}

void IIC_Start(void)
{
    SDA_OUT();
    IIC_Set_Sda(1);
    IIC_Set_Scl(1);

    HAL_Delay_Us(4);
    IIC_Set_Sda(0);

    HAL_Delay_Us(4);
    IIC_Set_Scl(0);
}

void IIC_Stop(void)
{
    SDA_OUT();
    IIC_Set_Sda(0);
    IIC_Set_Scl(0);

    HAL_Delay_Us(4);
    IIC_Set_Scl(1);
    HAL_Delay_Us(4);
    IIC_Set_Sda(1);
}

uint8_t IIC_Wait_Ack(void)
{
    uint8_t err_time = 0;
    SDA_IN();
    IIC_Set_Sda(1);
    HAL_Delay_Us(1);
    IIC_Set_Scl(1);
    HAL_Delay_Us(1);

    while(IIC_Read_Sda())
    {
        err_time++;
        if(err_time > 250)
        {
            IIC_Stop();
            return 1;
        }
    }
    IIC_Set_Scl(0);
    return 0;
}

void IIC_Ack(void)
{
    IIC_Set_Scl(0);
    SDA_OUT();
    IIC_Set_Sda(0);
    HAL_Delay_Us(2);
    IIC_Set_Scl(1);
    HAL_Delay_Us(2);
    IIC_Set_Scl(0);
}

void IIC_NAck(void)
{
    IIC_Set_Scl(0);
    SDA_OUT();
    IIC_Set_Sda(1);
    HAL_Delay_Us(5);
    IIC_Set_Scl(1);
    HAL_Delay_Us(5);
    IIC_Set_Scl(0);
}

void IIC_Send_Byte(uint8_t txd)
{                        
    uint8_t t;   
	SDA_OUT(); 	    
    IIC_Set_Scl(0);
    for(t=0;t<8;t++)
    {              
        IIC_Set_Sda((txd >> (7 - t)) & 1);
		HAL_Delay_Us(5);   
		IIC_Set_Scl(1);
		HAL_Delay_Us(5); 
		IIC_Set_Scl(0);	
		HAL_Delay_Us(5);
    }	 
} 	

uint8_t IIC_Read_Byte(unsigned char ack)
{
	unsigned char i,receive=0x07;
	SDA_IN();//SDA设置为输入
    for(i=0;i<8;i++ )
	{
        IIC_Set_Scl(0); 
        HAL_Delay_Us(10);
        IIC_Set_Scl(1); 
        receive = receive << 1;
        if(IIC_Read_Sda() == 1)   
        {
            receive = receive | 0x01;
        }
        else 
        {
            receive = receive & 0xfe;
        }
		HAL_Delay_Us(10); 
    }					 
    /* if (!ack)
        IIC_NAck();//发送nACK
    else
        IIC_Ack();  *///发送ACK   
    return receive;
}