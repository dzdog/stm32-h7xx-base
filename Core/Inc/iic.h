#ifndef __IIC_H_ 
#define __IIC_H_ 

#include "main.h"

#define GT911_SDA GPIO_PIN_6
#define GT911_SCL GPIO_PIN_7


void SDA_IN(void); 

void SDA_OUT(void);
void IIC_Set_Scl(uint8_t state);
void IIC_Set_Sda(uint8_t state);
uint8_t IIC_Read_Sda(void);
void IIC_Init(void);
void IIC_Start(void);
void IIC_Stop(void);
uint8_t IIC_Wait_Ack(void);
void IIC_Ack(void);
void IIC_NAck(void);
void IIC_Send_Byte(uint8_t txd);
uint8_t IIC_Read_Byte(unsigned char ack);
#endif // !"iic.h"
