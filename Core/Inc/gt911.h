#ifndef __GT911_H__
#define __GT911_H__

#include "main.h"
#include "usart.h"
#include "iic.h"

#define GT911_INT_PIN GPIO_PIN_9
#define GT911_RST_PIN GPIO_PIN_8

#define GT911_GPIO_PROT GPIOB

#define GT911_I2C_ADDRESS 0x5D

//#define GT911_CMD_W 0xBA
//#define GT911_CMD_R 0xBB
#define GT911_CMD_W 0x28
#define GT911_CMD_R 0x29


#define GT911_MAX_TOUCH 5



void GT911_Init(uint8_t addr);
uint8_t GT911_Write(uint16_t reg, uint8_t *data, uint8_t len);
void GT911_Read(uint16_t reg, uint8_t *data, uint8_t len);

#endif // __GT911_H__
